# Web application Zitate (backend)

_API based on HTTP requests to be used by the web application Zitate._



## Developed with 🛠️

_The RESTful API was developed with JavaScript with a functional programming approach. Data were storaged in MySQL database service. The following tools were used:_
* Visual Studio Code - IDE for code writing
* Node.js - JavaScript runtime built
* Express.js - Web framework for Node.js
* phpAdmin - MySQL database manager
* Postman - Platform for testing API



## Project date 📌

2022 - in progress
