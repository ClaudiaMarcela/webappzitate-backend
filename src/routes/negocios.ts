//DEFINIR LAS RUTAS DE LA APLICACIÓN CON RESPECTO A LA ENTIDAD "negocios" SEGÚN MOCKUPS

import { Router } from "express"; //Para el ruteo
import { obtenerNegocios, obtenerNegocio } from "../controllers/negociosControllers";

//Función: rutas hacia la entidad "negocios"
const negociosRoutes = (app) => {
    const router = Router(); //Crear un router para el manejo de muchas rutas
    app.use('/', router); //Para usar el router

    //Pruebas en Postman. Método GET
    //1. Mockup: listado negocios --> Mostrar informacion de todos los negocios
    //router.get('/obtenerNegocios', (req, res) => res.send("Obtener negocios desde negocios.ts"));

    
    //Enviar ruta y función CRUD (se encuentra en negociosControllers)
    //1. Mockup: listado negocios --> Mostrar informacion de todos los negocios
    router.get('/obtenerNegocios/:categoria', obtenerNegocios);

    router.get('/obtenerNegocio/:id', obtenerNegocio);
}

//Exportar la función "negociosRoutes" para ser usada en el archivo "app.ts"
export default negociosRoutes; //Export por defecto
