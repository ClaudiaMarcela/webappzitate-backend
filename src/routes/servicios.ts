//DEFINIR LAS RUTAS DE LA APLICACIÓN CON RESPECTO A LA ENTIDAD "servicios" SEGÚN MOCKUPS

import { Router } from "express"; //Para el ruteo
import { obtenerServicio, obtenerServicios } from "../controllers/serviciosControllers";

//Función: rutas hacia la entidad "servicios"
const serviciosRoutes = (app) => {
    const router = Router(); //Crear un router para el manejo de muchas rutas
    app.use('/', router); //Para usar el router

    //Pruebas en Postman. Método GET
    //1. Mockup: listado servicios --> Mostrar informacion de todos los servicios del negocio elegido
    //router.get('/obtenerServicios', (req, res) => res.send("Obtener servicios desde servicios.ts"));

    
    //Enviar ruta y función CRUD (se encuentra en negociosControllers)
    //1. Mockup: listado servicios --> Mostrar informacion de todos los servicios del negocio elegido
    router.get('/obtenerServicios', obtenerServicios);

    router.get('/obtenerServicio/:id', obtenerServicio);
}

//Exportar la función "serviciosRoutes" para ser usada en el archivo "app.ts"
export default serviciosRoutes; //Export por defecto
