import jwt from 'jsonwebtoken';

//Función: VALIDAR TOKEN
/*
Middlewares, antes de los métodos que se ejecutan requiriendo haber iniciado sesión
Propiedad del método: headers
Respuesta BackEnd: JSON (message: unauthorized / continua...)
*/
const validarToken = (req, res, next) => {
    const {token} = req.headers;
    if(token){
        jwt.verify(token, 'secretKey', (error, data) => {
            if(error){
                res.status(401).json({message: 'unauthorized'});
            }else{
                next();
            }
        });
    }else{
        res.status(401).json({message: 'unauthorized'});
    }
}

export {validarToken};