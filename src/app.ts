//2. Se actualiza el archivo package.json: contiene info acerca del proyecto tal como descripción, dependencias, scripts y licencia.
// Se agregan las claves "build" y "start"

//En esta carpeta "src" se guardan archivos de TypeScript
//3. Configuración del servidor: Importar express y guardar método express() en constante app. Hacer prueba inicial en Postman con app.get y del servidor con app.listen

import express, { response } from "express"; //Importar dependencia
import config from "./config/config"; //Importar variables de entorno
import negociosRoutes from "./routes/negocios";
import serviciosRoutes from "./routes/servicios";
import usuariosRoutes from "./routes/usuarios" //Importar función con rutas hacia la tabla "usuarios"


const app = express();


//Solucionar problema CORS (origen cruzado) cuando se conecta al FrontEnd, cada uno corriendo en un puerto diferente
app.use((_, res, next) => {
    res.header('Access-Control-Allow-origin', '*');
    res.header('Access-Control-Allow-Headers', '*');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});


//Para obtener el body en JSON. Ya no sería necesario descargar una biblioteca adicional
app.use(express.json());
app.use(express.urlencoded({extended: true}));

//Llamar funciones "usuariosRoutes" y "negociosRoutes" y enviar parámetro "app"
usuariosRoutes(app);
negociosRoutes(app);
serviciosRoutes(app);


//Prueba inicial para realizar en postman
app.get('/prueba', (req, res) => {
    res.send("Prueba del servidor")
});

//Prueba inicial para revisar conexión al servidor
//const port = 3000;
app.listen(config.PORT, () => {
    return console.log(`Servidor corriendo sobre el puerto ${config.PORT}`)
});

