//FUNCIONES CRUD DE LA TABLA "servicios" DE LA BASE DE DATOS

import executeQuery from "../services/mysql.service";


//Función: OBTENER SERVICIO
/*
Método FrontEnd: GET
Propiedad del método: params
Respuesta BackEnd: JSON (mensaje, datos de BD)
*/
const obtenerServicio = async(req, res) => {
    const{id} = req.params;
    try{
        const response = await executeQuery(`SELECT * FROM servicios WHERE id_servicio = ${id}`);
        const data = { //JSON
            message: `${response.length} datos encontrados`,
            datos: response.length > 0 ? response[0]: null
        }
        res.json(data);
    }catch(error){
        console.log(error);
        res.status(500).send(error);
    }
}


//Función: OBTENER SERVICIOS
/*
Método FrontEnd: GET
Propiedad del método: query
Respuesta BackEnd: JSON (mensaje, datos de BD)
*/
const obtenerServicios = async(req, res) => {
    const{negocio} = req.query;    
    try{
        const response = await executeQuery(`SELECT * FROM servicios WHERE id_negocio = ${negocio}`);
        const data = { //JSON
            message: `${response.length} datos encontrados`,
            datos: response.length > 0 ? response: null
        }
        res.json(data);
    }catch(error){
        console.log(error);
        res.status(500).send(error);
    }
}


//Exportar múltiples funciones para ser usadas en el archivo "negocios.ts"
export {obtenerServicio, obtenerServicios}
