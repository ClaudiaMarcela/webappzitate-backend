//FUNCIONES CRUD DE LA TABLA "negocios" DE LA BASE DE DATOS

import executeQuery from "../services/mysql.service";


//Función: OBTENER NEGOCIO
/*
Método FrontEnd: GET
Propiedad del método: params
Respuesta BackEnd: JSON (mensaje, datos de BD)
*/
const obtenerNegocio = async(req, res) => {
    const{id} = req.params;
    try{
        const response = await executeQuery(`SELECT * FROM negocios WHERE id_negocio = ${id}`);
        const data = { //JSON
            message: `${response.length} datos encontrados`,
            datos: response.length > 0 ? response[0]: null
        }
        res.json(data);
    }catch(error){
        console.log(error);
        res.status(500).send(error);
    }
    //res.send("Obtener negocio desde negociosControllers");
}


//Función: OBTENER NEGOCIOS
/*
Método FrontEnd: GET
Propiedad del método: params
Respuesta BackEnd: JSON (mensaje, datos de BD)
*/
const obtenerNegocios = async(req, res) => {
    const {categoria} = req.params;
    try{
        const response = await executeQuery(`SELECT * FROM negocios WHERE categoria_negocio = '${categoria}'`);
        const data = { //JSON
            message: `${response.length} datos encontrados`,
            datos: response.length > 0 ? response: null
        }
        res.json(data);
    }catch(error){
        console.log(error);
        res.status(500).send(error);
    }
    //res.send("Obtener negocio desde negociosControllers");
}


//Exportar múltiples funciones para ser usadas en el archivo "negocios.ts"
export {obtenerNegocio, obtenerNegocios}
