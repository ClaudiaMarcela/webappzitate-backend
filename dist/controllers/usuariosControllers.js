"use strict";
//FUNCIONES CRUD DE LA TABLA "usuarios" DE LA BASE DE DATOS
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.validarUsuario = exports.desactivarUsuario = exports.agregarUsuario = exports.actualizarUsuario = exports.obtenerUsuario = void 0;
const mysql_service_1 = __importDefault(require("../services/mysql.service"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
/*
Tener en cuenta:
1. El objeto "req" es por medio del cual se recibe la información desde el FrontEnd
2. El objeto "res" es por medio del cual se envía la respuesta hacia el FrontEnd
3. Se debe tener claro el tipo de petición HTTP que se va recibir (params, body -json, headers, query)
4. Se debe tener claro el tipo de respuesta que se va enviar (JSON, status)
*/
//Función: OBTENER USUARIO
/*
Método FrontEnd: GET
Propiedad del método: params
Respuesta BackEnd: JSON (message: X datos encontrados, datos: datos de BD)

Se desestructura el parámetro a recibir "id"
Se agrega un TRY-CATCH
Si esta bien:
Se guarda la consulta en la constante "response"
Se guarda el mensaje a mostrar en la constante "data"
Se devuelve un JSON con la información de la constante "data"
Si está mal:
Se devuelve el status 500 --> Condición inesperada
*/
const obtenerUsuario = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params; //Desestructuración del parametro recibido
    try {
        const response = yield (0, mysql_service_1.default)(`SELECT * FROM usuarios WHERE id_usuario = ${id}`);
        const data = {
            message: `${response.length} datos encontrados`,
            datos: response.length > 0 ? response[0] : null
        };
        res.json(data);
    }
    catch (error) {
        console.log(error);
        res.status(500).send(error);
    }
    //res.send("Obtener usuario desde usuariosControllers");
});
exports.obtenerUsuario = obtenerUsuario;
//Función: AGREGAR USUARIO
/*
Método FrontEnd: POST
Propiedad del método: body -Json
Respuesta BackEnd: JSON (message: created, id: idAgregado / message: correo existente)
*/
const agregarUsuario = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { nombre, apellido, telefono, email, status, password, identificacion, tipo, recuperar, rol } = req.body; //Desestructuración
    try {
        const existir = yield (0, mysql_service_1.default)(`SELECT * FROM usuarios WHERE email_usuario = '${email}'`);
        if (existir.length > 0) {
            res.json({ message: 'correo existente' });
        }
        else {
            const response = yield (0, mysql_service_1.default)(`INSERT INTO usuarios (nombre_usuario, apellido_usuario, telefono_usuario, email_usuario, status_usuario, password_usuario, identificacion_usuario, tipoIdentificacion_usuario, recuperar_password_usuario, rol_usuario) VALUES ('${nombre.toUpperCase()}', '${apellido.toUpperCase()}', '${telefono}', '${email.toLowerCase()}', '${status}', '${password}', '${identificacion}', '${tipo}', ${recuperar}, '${rol}')`);
            res.status(201).json({ message: "created", id: response.insertId }); //insertId = Campo auto_increment
        }
    }
    catch (error) {
        console.log(error);
        res.status(500).send(error);
    }
    //res.send("Agregar usuario desde usuariosControllers");
});
exports.agregarUsuario = agregarUsuario;
//Función: ACTUALIZAR USUARIO
/*
Método FrontEnd: PUT
Propiedad del método: body -Json, params -URL
Respuesta BackEnd: JSON (message: updated / message: not found )

Se desestructuran los parámetros a recibir del body (todos los campos) y del param (id)
Se agrega un TRY-CATCH
Si esta bien:
Se guarda la consulta en la constante "response" usando los parámetros desestructurados
Se muestra en consola el registro actualizado
Se valida si se actualizó o no. Si es verdadero, se envia un mensaje JSON diciendo "actualizado" y el id del registro. Si es falso, se envía el status 404 - Not found y un mensaje "no se encontró id ..."
Si está mal:
Se devuelve el status 500 --> Condición inesperada
*/
const actualizarUsuario = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { nombre, apellido, telefono, email, status, password, identificacion, tipo, recuperar, rol } = req.body; //Desestructuración del body
    //const {id} = req.params; //Desestructuración de params
    try {
        const response = yield (0, mysql_service_1.default)(`UPDATE usuarios SET nombre_usuario = '${nombre}', apellido_usuario = '${apellido}', telefono_usuario = '${telefono}', email_usuario = '${email}', status_usuario = '${status}', password_usuario = '${password}', identificacion_usuario = '${identificacion}', tipoIdentificacion_usuario = '${tipo}', recuperar_password_usuario = ${recuperar}, rol_usuario = '${rol}' WHERE id_usuario = ${req.params.id}`);
        console.log(response);
        if (response.affectedRows > 0) { //Si hubo actualización
            res.json({ message: 'updated' });
        }
        else {
            res.status(404).json({ message: 'not found' });
        }
    }
    catch (error) {
        console.log(error);
        console.log(nombre);
        res.status(500).send(error);
    }
    //res.send("Actualizar usuario desde usuariosControllers");
});
exports.actualizarUsuario = actualizarUsuario;
//Función: DESACTIVAR USUARIO (alterar campo "status" = "Inactivo")
/*
Método FrontEnd: PUT
Propiedad del método: params -URL
Respuesta BackEnd: JSON (message: desactivated, id: idusuario / message: not found, id: idEnviado)

Se desestructuran el parámetro a recibir "id" - params
Se agrega un TRY-CATCH
Si esta bien:
Se guarda la consulta en la constante "response" usando los parámetros desestructurados
Se muestra en consola el registro actualizado
Se valida si se actualizó o no. Si es verdadero, se envia un mensaje JSON diciendo "actualizado" y el id del registro. Si es falso, se envía el status 404 - Not found y un mensaje "no se encontró id ..."
Si está mal:
Se devuelve el status 500 --> Condición inesperada
*/
const desactivarUsuario = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params; //Desestructuración de params
    try {
        const response = yield (0, mysql_service_1.default)(`UPDATE usuarios SET status_usuario = "Inactivo" WHERE id_usuario = ${id}`);
        console.log(response);
        if (response.affectedRows > 0) { //Si hubo actualización
            res.json({ message: "desactivated", id: id });
        }
        else {
            res.status(404).json({ message: 'not found', id: id });
        }
    }
    catch (error) {
        console.log(error);
        res.status(500).send(error);
    }
    //res.send("Eliminar usuario desde usuariosControllers");
});
exports.desactivarUsuario = desactivarUsuario;
//Función: VALIDAR USUARIO
/*
Método FrontEnd: POST
Propiedad del método: body
Respuesta BackEnd: JSON (message: correct, user: datos del usuario / message: incorrect / message: not found)
*/
const validarUsuario = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { email, password } = req.body; //Desestructuración del parametro recibido
    try {
        const response = yield (0, mysql_service_1.default)(`SELECT password_usuario, id_usuario, nombre_usuario FROM usuarios WHERE email_usuario = '${email.toLowerCase()}'`);
        if (response.length > 0) { //Si encontró el registro
            if (response[0].password_usuario === password) {
                //res.json({message: 'correct'});
                jsonwebtoken_1.default.sign(response[0], 'secretKey', (error, token) => {
                    if (error) {
                        console.log(error);
                    }
                    else {
                        res.json({ message: 'correct', user: Object.assign(Object.assign({}, response[0]), { token }) });
                    }
                });
            }
            else {
                res.json({ message: 'incorrect' });
            }
        }
        else {
            res.json({ message: 'not found' });
        }
    }
    catch (error) {
        console.log(error);
        res.status(500).send(error);
    }
    //res.send("Validar usuario desde usuariosControllers");
});
exports.validarUsuario = validarUsuario;
//FUNCIÓN ADICIONAL: para poner en mayúscula la primera letra
function capitalizeFirstLetter(value) {
    return value.charAt(0).toUpperCase() + value.slice(1);
}
//# sourceMappingURL=usuariosControllers.js.map