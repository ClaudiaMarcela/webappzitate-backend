"use strict";
//FUNCIONES CRUD DE LA TABLA "servicios" DE LA BASE DE DATOS
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.obtenerServicios = exports.obtenerServicio = void 0;
const mysql_service_1 = __importDefault(require("../services/mysql.service"));
//Función: OBTENER SERVICIO
/*
Método FrontEnd: GET
Propiedad del método: params
Respuesta BackEnd: JSON (mensaje, datos de BD)
*/
const obtenerServicio = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    try {
        const response = yield (0, mysql_service_1.default)(`SELECT * FROM servicios WHERE id_servicio = ${id}`);
        const data = {
            message: `${response.length} datos encontrados`,
            datos: response.length > 0 ? response[0] : null
        };
        res.json(data);
    }
    catch (error) {
        console.log(error);
        res.status(500).send(error);
    }
});
exports.obtenerServicio = obtenerServicio;
//Función: OBTENER SERVICIOS
/*
Método FrontEnd: GET
Propiedad del método: query
Respuesta BackEnd: JSON (mensaje, datos de BD)
*/
const obtenerServicios = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { negocio } = req.query;
    try {
        const response = yield (0, mysql_service_1.default)(`SELECT * FROM servicios WHERE id_negocio = ${negocio}`);
        const data = {
            message: `${response.length} datos encontrados`,
            datos: response.length > 0 ? response : null
        };
        res.json(data);
    }
    catch (error) {
        console.log(error);
        res.status(500).send(error);
    }
});
exports.obtenerServicios = obtenerServicios;
//# sourceMappingURL=serviciosControllers.js.map