"use strict";
//FUNCIONES CRUD DE LA TABLA "negocios" DE LA BASE DE DATOS
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.obtenerNegocios = exports.obtenerNegocio = void 0;
const mysql_service_1 = __importDefault(require("../services/mysql.service"));
//Función: OBTENER NEGOCIO
/*
Método FrontEnd: GET
Propiedad del método: params
Respuesta BackEnd: JSON (mensaje, datos de BD)
*/
const obtenerNegocio = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    try {
        const response = yield (0, mysql_service_1.default)(`SELECT * FROM negocios WHERE id_negocio = ${id}`);
        const data = {
            message: `${response.length} datos encontrados`,
            datos: response.length > 0 ? response[0] : null
        };
        res.json(data);
    }
    catch (error) {
        console.log(error);
        res.status(500).send(error);
    }
    //res.send("Obtener negocio desde negociosControllers");
});
exports.obtenerNegocio = obtenerNegocio;
//Función: OBTENER NEGOCIOS
/*
Método FrontEnd: GET
Propiedad del método: params
Respuesta BackEnd: JSON (mensaje, datos de BD)
*/
const obtenerNegocios = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { categoria } = req.params;
    try {
        const response = yield (0, mysql_service_1.default)(`SELECT * FROM negocios WHERE categoria_negocio = '${categoria}'`);
        const data = {
            message: `${response.length} datos encontrados`,
            datos: response.length > 0 ? response : null
        };
        res.json(data);
    }
    catch (error) {
        console.log(error);
        res.status(500).send(error);
    }
    //res.send("Obtener negocio desde negociosControllers");
});
exports.obtenerNegocios = obtenerNegocios;
//# sourceMappingURL=negociosControllers.js.map