"use strict";
//DEFINIR LAS RUTAS DE LA APLICACIÓN CON RESPECTO A LA ENTIDAD "usuarios" SEGÚN MOCKUPS
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express"); //Para el ruteo
const usuariosControllers_1 = require("../controllers/usuariosControllers");
//Función: rutas hacia la entidad "usuarios"
const usuariosRoutes = (app) => {
    const router = (0, express_1.Router)(); //Crear un router para el manejo de muchas rutas
    app.use('/', router); //Para usar el router
    //Pruebas en Postman. Método GET
    //1. Mockup: perfil --> Mostrar perfil del usuario
    //router.get('/obtenerUsuario/:id', (req, res) => res.send("Obtener usuario desde usuarios.ts"));
    //2. Mockup: perfil --> Actualizar información del usuario
    //router.get('/actualizarUsuario/:id', (req, res) => res.send("Actualizar usuario desde usuarios.ts"));
    //3. Mockup: registrarse --> Agregar nuevo usuario
    //router.get('/agregarUsuario', (req, res) => res.send("Agregar usuario desde usuarios.ts"));
    //4. Mockup: perfil --> Dar de baja
    //router.get('/eliminarUsuario/:id', (req, res) => res.send("Eliminar usuario desde usuarios.ts"));
    //5. Mockup: iniciar sesion --> Validar credenciales
    //router.get('/validarUsuario', (req, res) => res.send("Validar usuario desde usuarios.ts"));
    //Enviar ruta y función CRUD (se encuentra en negociosControllers)
    //1. Mockup: perfil --> Mostrar perfil del usuario
    router.get('/obtenerUsuario/:id', usuariosControllers_1.obtenerUsuario);
    //2. Mockup: perfil --> Actualizar información del usuario
    router.put('/actualizarUsuario/:id', usuariosControllers_1.actualizarUsuario);
    //3. Mockup: registrarse --> Agregar nuevo usuario
    router.post('/agregarUsuario', usuariosControllers_1.agregarUsuario);
    //4. Mockup: perfil --> Dar de baja --> Se cambia el "status" de "Activo" a "Inactivo"
    router.put('/desactivarUsuario/:id', usuariosControllers_1.desactivarUsuario);
    //5. Mockup: iniciar sesión --> Validar credenciales
    router.post('/validarUsuario', usuariosControllers_1.validarUsuario);
    //6. Mockup: ingresar --> Recuperar contraseña 
};
//Exportar la función "usuariosRoutes" para ser usada en el archivo "app.ts"
exports.default = usuariosRoutes; //Export por defecto
//# sourceMappingURL=usuarios.js.map