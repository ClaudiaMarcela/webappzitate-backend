"use strict";
//DEFINIR LAS RUTAS DE LA APLICACIÓN CON RESPECTO A LA ENTIDAD "servicios" SEGÚN MOCKUPS
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express"); //Para el ruteo
const serviciosControllers_1 = require("../controllers/serviciosControllers");
//Función: rutas hacia la entidad "servicios"
const serviciosRoutes = (app) => {
    const router = (0, express_1.Router)(); //Crear un router para el manejo de muchas rutas
    app.use('/', router); //Para usar el router
    //Pruebas en Postman. Método GET
    //1. Mockup: listado servicios --> Mostrar informacion de todos los servicios del negocio elegido
    //router.get('/obtenerServicios', (req, res) => res.send("Obtener servicios desde servicios.ts"));
    //Enviar ruta y función CRUD (se encuentra en negociosControllers)
    //1. Mockup: listado servicios --> Mostrar informacion de todos los servicios del negocio elegido
    router.get('/obtenerServicios', serviciosControllers_1.obtenerServicios);
    router.get('/obtenerServicio/:id', serviciosControllers_1.obtenerServicio);
};
//Exportar la función "serviciosRoutes" para ser usada en el archivo "app.ts"
exports.default = serviciosRoutes; //Export por defecto
//# sourceMappingURL=servicios.js.map