"use strict";
//DEFINIR LAS RUTAS DE LA APLICACIÓN CON RESPECTO A LA ENTIDAD "negocios" SEGÚN MOCKUPS
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express"); //Para el ruteo
const negociosControllers_1 = require("../controllers/negociosControllers");
//Función: rutas hacia la entidad "negocios"
const negociosRoutes = (app) => {
    const router = (0, express_1.Router)(); //Crear un router para el manejo de muchas rutas
    app.use('/', router); //Para usar el router
    //Pruebas en Postman. Método GET
    //1. Mockup: listado negocios --> Mostrar informacion de todos los negocios
    //router.get('/obtenerNegocios', (req, res) => res.send("Obtener negocios desde negocios.ts"));
    //Enviar ruta y función CRUD (se encuentra en negociosControllers)
    //1. Mockup: listado negocios --> Mostrar informacion de todos los negocios
    router.get('/obtenerNegocios/:categoria', negociosControllers_1.obtenerNegocios);
    router.get('/obtenerNegocio/:id', negociosControllers_1.obtenerNegocio);
};
//Exportar la función "negociosRoutes" para ser usada en el archivo "app.ts"
exports.default = negociosRoutes; //Export por defecto
//# sourceMappingURL=negocios.js.map