"use strict";
//2. Se actualiza el archivo package.json: contiene info acerca del proyecto tal como descripción, dependencias, scripts y licencia.
// Se agregan las claves "build" y "start"
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
//En esta carpeta "src" se guardan archivos de TypeScript
//3. Configuración del servidor: Importar express y guardar método express() en constante app. Hacer prueba inicial en Postman con app.get y del servidor con app.listen
const express_1 = __importDefault(require("express")); //Importar dependencia
const config_1 = __importDefault(require("./config/config")); //Importar variables de entorno
const negocios_1 = __importDefault(require("./routes/negocios"));
const servicios_1 = __importDefault(require("./routes/servicios"));
const usuarios_1 = __importDefault(require("./routes/usuarios")); //Importar función con rutas hacia la tabla "usuarios"
const app = (0, express_1.default)();
//Solucionar problema CORS (origen cruzado) cuando se conecta al FrontEnd, cada uno corriendo en un puerto diferente
app.use((_, res, next) => {
    res.header('Access-Control-Allow-origin', '*');
    res.header('Access-Control-Allow-Headers', '*');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});
//Para obtener el body en JSON. Ya no sería necesario descargar una biblioteca adicional
app.use(express_1.default.json());
app.use(express_1.default.urlencoded({ extended: true }));
//Llamar funciones "usuariosRoutes" y "negociosRoutes" y enviar parámetro "app"
(0, usuarios_1.default)(app);
(0, negocios_1.default)(app);
(0, servicios_1.default)(app);
//Prueba inicial para realizar en postman
app.get('/prueba', (req, res) => {
    res.send("Prueba del servidor");
});
//Prueba inicial para revisar conexión al servidor
//const port = 3000;
app.listen(config_1.default.PORT, () => {
    return console.log(`Servidor corriendo sobre el puerto ${config_1.default.PORT}`);
});
//# sourceMappingURL=app.js.map