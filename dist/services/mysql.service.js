"use strict";
//GESTIONAR CONEXIÓN A LA BASE DE DATOS
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mysql2_1 = __importDefault(require("mysql2"));
const config_1 = __importDefault(require("../config/config"));
//Función: OBTENER CONEXIÓN
const getConnection = () => {
    //Crear constante "connection"
    const connection = mysql2_1.default.createConnection({
        database: config_1.default.DATABASE,
        user: config_1.default.DB_USER,
        password: config_1.default.DB_PASSWORD,
        host: config_1.default.DB_HOST,
        port: +config_1.default.DB_PORT //Se recibe como String, pero espera un número
    });
    //Ejecutar método "connect"
    connection.connect((error) => {
        if (error) {
            throw error; //Lanza error
        }
        else {
            console.log("Sucessful connection"); //Muestra mensaje: todo ok
        }
    });
    return connection;
};
//Función: EJECUTAR CONSULTA
const executeQuery = (query) => {
    return new Promise((resolve, reject) => {
        try {
            const connection = getConnection(); //Llamar función: obtener conexión
            connection.query(query, (error, result) => {
                if (error) {
                    reject(error);
                }
                else {
                    resolve(result);
                }
            });
            connection.end(); //Cerrar conexión
        }
        catch (error) {
            console.log(error);
            reject(error);
        }
    });
};
//Exportar la función "executeQuery" para ser utilizada en las funciones del CRUD
exports.default = executeQuery; //Export por defecto
//# sourceMappingURL=mysql.service.js.map