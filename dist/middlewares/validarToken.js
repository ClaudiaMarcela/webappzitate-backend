"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.validarToken = void 0;
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
//Función: VALIDAR TOKEN
/*
Middlewares, antes de los métodos que se ejecutan requiriendo haber iniciado sesión
Propiedad del método: headers
Respuesta BackEnd: JSON (message: unauthorized / continua...)
*/
const validarToken = (req, res, next) => {
    const { token } = req.headers;
    if (token) {
        jsonwebtoken_1.default.verify(token, 'secretKey', (error, data) => {
            if (error) {
                res.status(401).json({ message: 'unauthorized' });
            }
            else {
                next();
            }
        });
    }
    else {
        res.status(401).json({ message: 'unauthorized' });
    }
};
exports.validarToken = validarToken;
//# sourceMappingURL=validarToken.js.map